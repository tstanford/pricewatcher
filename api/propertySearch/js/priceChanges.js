(function(){

    var priceChangesDirective = function(){

        var priceChangesController = function($scope, propertyService){
            
            $scope.visible = false;
            
            $scope.showchanges = function(){
                $scope.visible = true;
                $scope.loading = true;
                propertyService.getPriceChanges($scope.refid, function(priceChanges){
                    for(var i=0;i<priceChanges.length;i++) {
                        priceChanges[i].oldPrice = priceChanges[i].oldPrice.toLocaleString();
                        priceChanges[i].newPrice = priceChanges[i].newPrice.toLocaleString();
                        priceChanges[i].dateChanged = new Date(priceChanges[i].dateChanged).toLocaleString();
                    }
                    
                    $scope.priceChanges = priceChanges;
                    $scope.loading = false;
                });
            };
        };

        return {
            controller: priceChangesController,
            templateUrl: "templates/priceChanges.html",
            restrict: 'EA',
            scope: {
                refid: '='
            },
        };
    };

    angular.module("myModule").directive("pricechanges", priceChangesDirective);
    
})();
