(function(){
    
    angular.module("myModule").factory("propertyService", function($http) {
        
        var service = {};       
        
        service.getAll = function(callback){            
            $http.get('/api/properties/all').success(callback);
        };
        
        service.query = function(description, address, minprice, maxprice, callback){
            $http.get('/api/properties?description='+description+'&address='+address+'&minprice='+minprice+'&maxprice='+maxprice).success(callback);
        };
        
        service.getStats = function(callback){
            $http.get('/api/runs/latest').success(callback);
        };
        
        service.getPriceChanges = function(refId, callback) {
            $http.get('/api/pricechanges/'+refId).success(callback);
        };
        
        return service;
    })
    
})();
