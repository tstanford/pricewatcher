/* global asc */
(function(){
    
    var sortByPriority = function(){
        return sortByProperty("priority", true);
    };
    
    var sortByPrice = function(asc){
        return sortByProperty("price", asc);
    };
    
    var sortByProperty = function(property, asc){
        return function(a,b){
            if(a[property] < b[property]) return asc?-1:1;
            if(a[property] > b[property]) return asc?1:-1;
            return 0;                        
        };  
    }
    
    var sortBySource = function(asc){
        return sortByProperty("source", asc);
    };
    
    var sortByAddedDate = function(asc){
        return sortByProperty("dateAdded", asc);
    };
    
    var propertiesDirective = function(){

        var propertiesController = function($scope, propertyService){        
            
            var now = Date.now();            
            var savedSearch = getSavedSearch(); 
            
            $scope.description = savedSearch.description;
            $scope.address = savedSearch.address;
            $scope.min=savedSearch.min;
            $scope.max=savedSearch.max;  
            
            $scope.sort = {
                byPrice : {asc: true, selected:false},
                bySource: {asc: true, selected:false},
                byAddedDate: {asc: true, selected:false},
                byDefault: {asc: true, selected:false},
            };   
            
            var selectSort = function(sortOption){
                for(var key in $scope.sort) {
                    if(key === sortOption) {
                        $scope.sort[key].selected = true;
                        $scope.sort[key].asc = !$scope.sort[key].asc;   
                    }
                    else {
                        $scope.sort[key].selected = false;
                    }
                }
            };
            
            $scope.sortByPrice = function(){
                $scope.items = $scope.items.sort(sortByPrice($scope.sort.byPrice.asc));
                selectSort("byPrice");
            }
            
            
            $scope.sortByDefault = function(){
                $scope.items = $scope.items.sort(sortByPrice(false)).sort(sortByPriority());
                selectSort("byDefault");
            };
            
            $scope.sortBySource = function(){
                $scope.items = $scope.items.sort(sortBySource($scope.sort.bySource.asc));
                selectSort("bySource");
            }
            
            $scope.sortByAddedDate = function(){
                $scope.items = $scope.items.sort(sortByAddedDate($scope.sort.byAddedDate.asc));
                selectSort("byAddedDate");
            }
            
            $scope.getStats = function(){
                $scope.statsloaded = false;    
                propertyService.getStats(function(stats){
                    $scope.stats = {};
                    $scope.stats.date = new Date(stats.runDate).toLocaleString();
                    $scope.stats.added = stats.addCount;
                    $scope.stats.removed = stats.deleteCount;
                    $scope.stats.updated = stats.updateCount;
                    $scope.stats.seen = stats.seenCount;
                    $scope.statsloaded = true;
                });                
            };
        
            $scope.search = function(description, address, min, max){
                if((address||"").length==0) {
                    return;
                }
                $scope.loading = true;
                propertyService.query(description||"", address, min||0, max||20000000, function(data){
                
                    data.forEach(function(item){
                        
                        item.age = (now-item.dateAdded)/1000/60/60/24;
                        item.changeAge = (now-item.lastUpdated)/1000/60/60/24;                   
                        item.priority = (item.age <1) ? 0: (item.priceChanges > 0 && item.changeAge < 1) ? 1 : 2;
                        
                        if(item.source === "espc"){
                            item.url = "https://espc.com/properties/details.aspx?pid="+item.refId;
                        }
                        if(item.source === "rightmove"){
                            item.url = "http://www.rightmove.co.uk/commercial-property-for-sale/property-"+item.refId.substr(2)+".html";
                        }
                        if(item.source === "yourmove"){
                            item.url = "https://www.your-move.co.uk/property/"+item.refId;
                        }
                        if(item.source === "remax"){
                            item.url = "http://rack01.powering2.expertagent.co.uk/%28S%285yz5lfngrroqsnn0qjucovic%29%29/propertyDetails2.aspx?aid={a9d512bf-391f-478b-85a1-4d44bf35d656}&pid="+item.refId;
                        }
                        
                        item.dateAddedText = new Date(item.dateAdded).toLocaleString();
                    }); 
                
                    $scope.items = data.sort(sortByPrice(false)).sort(sortByPriority());      
                    storeSavedSearch(description, address, min, max);                   
                    $scope.loading = false;
                    selectSort("byDefault");
                });
            };
            setInterval(function(){$scope.getStats()}, 1800000);            
            
            $scope.search(savedSearch.description, savedSearch.address, savedSearch.min, savedSearch.max);
            $scope.loading = false;
            $scope.statsloaded = false;          
            $scope.getStats();

        };

        return {
            controller: propertiesController,
            templateUrl: "templates/properties.html",
            restrict: 'EA'
        };
    };
    
    var getSavedSearch = function(){
        var savedSearch = localStorage.getItem("savedSearch");            
        if(savedSearch) {
            savedSearch = JSON.parse(savedSearch);
        } else {
            savedSearch = {
                description: "3 bed",
                address: "Dunfermline",
                min: 95000,
                max: 160000
            };
        }
        
        return savedSearch;     
    };
    
    var storeSavedSearch = function(description, address, min, max){
        localStorage.setItem("savedSearch", JSON.stringify({
            description: description,
            address: address,
            min: min,
            max: max
        }));
    };

    angular.module("myModule").directive("properties", propertiesDirective);
    
    var propertyDetailsDirective = function(){
        return {
            templateUrl: "templates/propertyDetails.html",
            restrict: 'EA'
        };
    };    
    angular.module("myModule").directive("propertyDetails", propertyDetailsDirective);
    
})();
