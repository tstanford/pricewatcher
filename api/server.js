var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var repo = require("../pwrepository.js").propertyWatcherRepository;

app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = 80;
var router = express.Router();

router.get('/properties/all', function(req, res, next) {
    var db = new repo("../data.db", function(){
        this.getAllProperties(function(result){
            res.json(result);
            next();
        });
    });
});

router.get('/properties', function(req, res, next) {
    var db = new repo("../data.db", function(){
        this.getProperties(req.query.description.split(" "), req.query.address, req.query.minprice, req.query.maxprice, function(result){
            res.json(result);
            next();
        });
    });
});

router.get('/priceChanges/:refId', function(req, res, next) {
    var db = new repo("../data.db", function(){
        this.getPriceChanges(req.params.refId, function(result){
            res.json(result);
            next();
        });
    });
});

router.get('/runs/latest', function(req, res, next){
    var db = new repo("../data.db", function(){
        this.getLatestRun(function(result){
            res.json(result);
            next();
        });
    });
});


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use("/api", router);
app.use("/favicon.ico", express.static("favicon.ico"));
app.use("/search", express.static("propertySearch/"));
app.listen(port);

