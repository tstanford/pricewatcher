var results = [];
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,        // do not load images
        loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
    }
});
var pageNumber = 0;

casper.options.waitTimeout = 60000;

phantom.addCookie({
    "name": "permuserid",
    "value": "150904MIOPK46A6BBSI4MK10JN2DYZOI",
    "domain": ".rightmove.co.uk"});

// casper.start('http://www.rightmove.co.uk/property-for-sale/find.html?locationIdentifier=REGION%5E453&maxPrice=300000&minBedrooms=2&radius=5.0&googleAnalyticsChannel=buying&numberOfPropertiesPerPage=50', function() {
    // processPage.call(this);
// });

casper.start('file:///home/tim/projects/pricewatcher/rm2.html', function() {
    //processPage.call(this);
});

casper.run(function() {
    //this.echo(results.length);
    
    if(results.length === 0) {
        this.capture("rightmove_noresults.png");
        var fs = require('fs');
        fs.write("rightmove_noresults.html", this.getHTML(), 'w');
	this.die("rightmove: no results");
    }
    
    this.echo(JSON.stringify(results));
    this.exit();
});

var getResults = function(){   
    var parsePrice = function(price){
        var matches = price.match(/^(.*)£(.*)$/);       
        return {model: matches[1].trim(),price: parseInt(matches[2].replace(/,/g,""))};        
    }; 
    
    var parseID = function(aElement){
        var matches = aElement.match(/property-([0-9]+)\.html/);       
        return parseInt(matches[1]);
    }
    
    var rows = document.querySelectorAll("div.summarymaincontent");
    
    return Array.prototype.map.call(rows, function(row){
        var address = row.querySelector(".address>a>span:nth-of-type(2)").innerText;
        var price = parsePrice(row.querySelector("p.price>span").innerText);        
        var description = row.querySelector(".address>a>span:nth-of-type(1)").innerText;
        var firstLink = row.querySelectorAll("a")[0];
        
        var item = {
            refId: "RM"+parseID(firstLink.href),
            description: description,
            address: address,
            price: price.price,
            pricingModel: price.model,
            town: "",
            postcode: "",
            source: "rightmove"
        };
        
        return item;
    });
};

var isNextButtonAvail = function(){
    var items = document.querySelectorAll("a.pagenavigation");
    
    console.log(items);
    
    for(var i=0; i<items.length; i++){
        if(items[i].innerText === "next"){
            return true;
        }
    }
    
    return false;
}

var processPage = function(){  

    var that = this;
    
    //console.log("waiting for sitefooter");
     
    that.waitForText(/(policy|cookies)/i, function(){
        //console.log("found end of page");
        var pageResults = that.evaluate(getResults);
        results = results.concat(pageResults||[]);
        var nextAvailable = that.evaluate(isNextButtonAvail);
        if(nextAvailable){
            that.clickLabel("next", "a");
            processPage.call(that);
        }
  }, function(){
      that.capture("rightmove_error.png");
      var fs = require('fs');
      fs.write("rightmove_error.html", that.getHTML(), 'w');
      that.die("Rightmove: Never found end of page");
  }, 15000);
};
