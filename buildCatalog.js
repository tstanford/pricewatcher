#!/usr/local/bin/node
var async = require("async");
var exec = require('child_process').exec;
var repo = require("./pwrepository.js").propertyWatcherRepository;
var Promise = require('promise');
var changeReporter = require("./changeReporter.js");

var reporter = new changeReporter();
var scrapers = [
    "./espc_scrape.js",
    "./yourmove_scrape.js",
    "./remax_scrape.js",
    "./rightmove_scrape.js"
];

var getData = function(script, callback){
    var data = [];    
    var child = exec(script, {maxBuffer: 1024 * 50000},
      function (error, stdout, stderr) {     
        try{
            data = data.concat(JSON.parse(stdout));        
        }
        catch(e) {
            console.log(script+' error: ' + e);
            throw(e);
        }
        if (error !== null) {
          console.log('exec error: ' + error);
        }
        else {
            callback(data);
        }
    });
};

var processData = function(db, data, runDateTime, processDone){    
    async.each(data, function(currentRow, callback){                    
        if(currentRow == null || currentRow.refId == null) {
            callback();
            return;
        }
        
        db.getProperty(currentRow.refId, function(existingRow){
            
            if(existingRow == null){
                currentRow.dateAdded = runDateTime;
                db.addProperty(currentRow, function(){
                    reporter.reportAdd(currentRow);
                    callback();
                });   
            }
            else {
                if(currentRow.pricingModel !== existingRow.pricingModel || currentRow.price !== existingRow.price) {
                    var difference = currentRow.price-existingRow.price;
                    db.addPriceChange({
                        refId: existingRow.refId,
                        dateChanged: runDateTime,
                        difference: difference,
                        oldPrice: existingRow.price,
                        oldPricingModel: existingRow.pricingModel,
                        newPrice: currentRow.price,
                        newPricingModel: currentRow.pricingModel
                    },function(){
                        db.updateProperty({
                            refId: existingRow.refId,
                            price: currentRow.price,
                            pricingModel: currentRow.pricingModel,
                            lastUpdated: runDateTime
                        }, function(){
                            reporter.reportUpdate(existingRow, currentRow, difference);
                            callback();
                        });
                    })
                }
                else {
                    db.updatePropertyAsSeen({
                        refId: existingRow.refId,
                        lastSeen: runDateTime
                    }, function(){
                        reporter.reportAsSeen(existingRow);
                        callback();
                    });
                }
            }                
            
        });
                 
    }, function(){
        //console.log("done processing");
        processDone();
    });
}

var finaliseRun = function(db, runDateTime){
    //console.log("done");
    if(reporter.hasReported === true){
        db.addRun(runDateTime, function(){
            console.log(runDateTime.getTime());
            db.close();
            process.exit(0);
        });
    }
    else {
        db.close();
        process.exit(1);
    } 
};

var run = function(){    
    var results = [];    
    var db = new repo("data.db", function(){
        var runDateTime = new Date();
        
        var newScraperTask = function(db, runDateTime, scraper){            
            return function(){
                return new Promise(function(resolve,reject){
                    //console.log("running scraper "+scraper);
                    getData("/usr/local/bin/casperjs --ssl-protocol=any "+scraper, 
                        function(data){
                            results = results.concat(data);
                            //console.log("scraper finished "+scraper);
                            //console.log(results.length);
                            resolve();
                        }
                    );
                }).catch(function(error) {
                    console.error(error.stack);
                    db.close();
                    process.exit(1);
                });
            }
        };
        
        var currentPromise = null;
        scrapers.forEach(function(scraper){
            //console.log("creating promise for scraper "+scraper);
            currentPromise = currentPromise || Promise.resolve();                        
            currentPromise = currentPromise.then(newScraperTask(db, runDateTime, scraper));
        });

        currentPromise.then(function(){
            processData(db, results, runDateTime, function(){
                finaliseRun(db, runDateTime)
            });
        });
    });
};

run();
