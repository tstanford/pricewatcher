var results = [];
var currentPage = 0;
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,        // do not load images
        loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
    },
    //verbose: true,
    //logLevel: "debug"
});

casper.options.waitTimeout = 20000;

var processResults = function(){
    
    var parseId = function(row){
        var splits = row.id.split("_");
        return parseInt(splits[1]);
    };    
    
    var rows = document.querySelectorAll("div.property");
    return Array.prototype.map.call(rows, function(row){
        
        try{  
            var address = row.querySelector("p.address").innerText;
            var price = parseInt(row.querySelector("h2.sale_price").innerText.replace(/[^\d]/g,""));
            var pricingModel = row.querySelector("h4.sale_pre_message").innerText;
            var description = row.querySelector("p.blurb").innerText;
            var addresslines = address.split(",");       
            var isSold = row.querySelector("span.sale_status") != null;

            var item = {
                refId: parseId(row),
                description: description.replace(" for sale",""),
                address: address,
                price: price,
                pricingModel: pricingModel,
                isSold: isSold,
                town: addresslines[addresslines.length-2].trim(),
                postcode: addresslines[addresslines.length-1].trim(),
                source: "yourmove"            
            };
            
            return item;
        }
        catch(err){
            return null;
        }
        
    }).filter(function(item){return item != null && item.isSold === false});
};

var loadAndWaitForMore = function(onDone){
    
    currentPage++;
    
    var that = this;
   
    that.waitForText("Load more properties",    
        function(){            
             //that.echo(currentPage);            
             that.click("#autopagi_not_so_auto_loader");
             that.waitFor(function(){return that.evaluate(
                function(){
                    return !document.querySelector("body").classList.contains("submitting");
             })},
                function(){
                    loadAndWaitForMore.call(that, onDone);
                }
             );
        },
        function(){
            onDone.call(that);
        },10000
     );
};

casper.start('https://www.your-move.co.uk/properties-for-sale/dunfermline-fife/!/sale_type/1/location/Dunfermline,%20Fife%7C252461016/price_from/0/radius/10/sale_agreed/0', function() {    

    var that = this;
    
    //console.log("page loading");

    loadAndWaitForMore.call(that, function(){
        results = that.evaluate(processResults||[])
    });
});

casper.run(function() {    
    //his.echo(results.length);
    this.echo(JSON.stringify(results));
    this.exit();
});
