var changeReporter = function(){
    this.hasReported = false;
};

changeReporter.prototype.reportAdd = function(data){
    this.hasReported = true;
    //console.log("N: "+data.address+" (ref:"+data.refId+") - Price "+data.price);    
};
changeReporter.prototype.reportUpdate = function(oldData,newData,difference){
    this.hasReported = true;
    //console.log("U: "+oldData.address+" (ref:"+oldData.refId+") - Price "+((difference<0)?"down":"up")+" from "+oldData.price+"("+oldData.pricingModel+") to "+newData.price+"("+newData.pricingModel+")");
};

changeReporter.prototype.reportAsSeen = function(data){
    this.hasReported = true;
    //console.log("U: "+oldData.address+" (ref:"+oldData.refId+") - Price "+((difference<0)?"down":"up")+" from "+oldData.price+"("+oldData.pricingModel+") to "+newData.price+"("+newData.pricingModel+")");
};

module.exports = changeReporter;
