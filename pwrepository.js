var sqlite3 = require('sqlite3').verbose();    

var propertyWatcherRepository = function(filename, callback){
    var db = new sqlite3.Database(filename);    
    this.db = db;    
    var that = this;    
    db.serialize(function() {
      db.run("CREATE TABLE IF NOT EXISTS properties (refId Text, dateAdded DateTime, lastUpdated DateTime, lastSeen DateTime, description Text, address Text, price Integer, pricingModel Text, town Text, postcode Text, source Text);");
      db.run("CREATE TABLE IF NOT EXISTS priceChanges (refId Text, dateChanged DateTime, oldPrice Integer, oldPricingModel Text, newPrice Integer, newPricingModel Text, difference Integer);");
      db.run("CREATE TABLE IF NOT EXISTS runs (runDate DateTime, dateText Text, addCount INTEGER, updateCount INTEGER, deleteCount INTEGER, seenCount INTEGER);");
      db.run("CREATE VIEW IF NOT EXISTS activeProperties as select * from properties where lastSeen in (select max(runDate) from runs)");
      db.run("CREATE VIEW IF NOT EXISTS priceChangeCounts as select refId, count(*)  as count from priceChanges group by refId;");
      db.run("CREATE VIEW IF NOT EXISTS activePropertiesWithChangeCounts as select p.*, coalesce(c.count,0) as priceChanges from activeProperties as p left outer join priceChangeCounts as c on c.refId = p.refId;");
      db.run("CREATE VIEW IF NOT EXISTS recentlyAddedProperties as select * from properties as p where  p.dateAdded in (select max(runDate) from runs);");
      db.run("CREATE VIEW IF NOT EXISTS recentlyUpdatedProperties as select * from properties as p where  p.lastUpdated in (select max(runDate) from runs) and p.dateAdded not in (select max(runDate) from runs);");
      db.run("CREATE VIEW IF NOT EXISTS recentlySeenProperties as select * from properties as p where  p.lastSeen in (select max(runDate) from runs) and p.dateAdded not in (select max(runDate) from runs) and p.lastUpdated not in (select max(runDate) from runs);");
      db.run("CREATE VIEW IF NOT EXISTS recentlyDeletedProperties as select * from properties as p where  p.lastSeen in (select runDate  from runs order by runDate desc limit 1 offset 1);");
      db.run("CREATE TRIGGER IF NOT EXISTS CreateRunCounts after insert on runs for each row begin update runs set addCount = (select count(*) from recentlyAddedProperties) where runDate in (select max(rundate) from runs); update runs set updateCount = (select count(*) from recentlyUpdatedProperties) where runDate in (select max(rundate) from runs); update runs set deleteCount = (select count(*) from recentlyDeletedProperties) where runDate in (select max(rundate) from runs); update runs set seenCount = (select count(*) from recentlySeenProperties) where runDate in (select max(rundate) from runs); end;")
      callback.call(that);
    });    
};

propertyWatcherRepository.prototype.addProperty = function(data, callback){    
    var that = this;    
    this.db.serialize(function(){
        this.run("INSERT INTO properties (refId, lastUpdated, lastSeen, dateAdded, description, address, price, pricingModel, town, postcode, source) values ($refId, $lastUpdated, $lastSeen, $dateAdded, $description, $address, $price, $pricingModel, $town, $postcode, $source)", {
            $refId: data.refId,
            $dateAdded: data.dateAdded,
            $description:data.description,
            $address:data.address,
            $price:data.price,
            $pricingModel:data.pricingModel,
            $town:data.town,
            $postcode:data.postcode,
            $lastUpdated: data.dateAdded,
            $lastSeen: data.dateAdded,
            $source: data.source
        }, function(){
            callback.call(that);
        });    
    });
};

propertyWatcherRepository.prototype.addRun = function(runDate, callback){    
    var that = this;    
    this.db.serialize(function(){
        this.run("INSERT INTO runs (runDate, dateText) values ($runDate, $dateText)", {
                $runDate: runDate,
                $dateText: runDate.toString(),
            }, function(){
                callback.call(that);
            });    
    });
};

propertyWatcherRepository.prototype.addPriceChange = function(data, callback){    
    var that = this;    
    this.db.serialize(function(){
        this.run("INSERT INTO priceChanges (refId, dateChanged, oldPrice, oldPricingModel, newPrice, newPricingModel, difference) values ($refId, $dateChanged, $oldPrice, $oldPricingModel, $newPrice, $newPricingModel, $difference)", {
                $refId: data.refId,
                $dateChanged: data.dateChanged,
                $oldPrice: data.oldPrice,
                $oldPricingModel: data.oldPricingModel,
                $newPrice: data.newPrice,
                $newPricingModel: data.newPricingModel,
                $difference: data.difference
            }, function(){
                callback.call(that);
            });    
    });
};

propertyWatcherRepository.prototype.updateProperty = function(data, callback){    
    var that = this;    
    this.db.run("update properties set price=$price,pricingModel=$pricingModel,lastUpdated=$lastUpdated, lastSeen=$lastSeen where refId=$refId", {
        $refId: data.refId,
        $price: data.price,
        $pricingModel: data.pricingModel,
        $lastUpdated: data.lastUpdated,
        $lastSeen: data.lastUpdated
        }, function(){
            callback.call(that);
    });
};

propertyWatcherRepository.prototype.updatePropertyAsSeen = function(data, callback){    
    var that = this;    
    this.db.run("update properties set lastSeen=$lastSeen where refId=$refId", {
        $refId: data.refId,
        $lastSeen: data.lastSeen
        }, function(){
            callback.call(that);
    });
};

propertyWatcherRepository.prototype.getAllProperties = function(callback){
    this.db.all("select * from activeProperties order by price desc", function(err, rows){callback(rows)});
};

propertyWatcherRepository.prototype.getProperties = function(description, address, minPrice, maxPrice, callback){
    
    var descriptionClause = "";
    
    if(description instanceof Array) {
        for(var i=0; i<description.length; i++) {
            descriptionClause += "description like \'%"+description[i].replace(/\'/g, "''")+"%\' and ";            
        }
    }
    else {
        descriptionClause = (description)?"description like \'%"+description.replace(/\'/g, "''")+"%\' and ":"";   
    }
    
    this.db.all("select * from activePropertiesWithChangeCounts where ("+descriptionClause+"address like \'%"+address.replace(/\'/g, "''")+"%\') and price >= "+parseInt(minPrice)+" and price <= "+parseInt(maxPrice)+" order by price desc", function(err, rows){callback(rows)});
};

propertyWatcherRepository.prototype.getAllPriceChanges = function(callback){
    this.db.all("select * from priceChanges", function(err, rows){callback(rows)});
};

propertyWatcherRepository.prototype.getPriceChanges = function(refId, callback){
    this.db.all("select * from priceChanges where refId='"+refId.replace(/\'/g, "''")+"' order by dateChanged", function(err, rows){callback(rows)});
};

propertyWatcherRepository.prototype.getProperty = function(refId, callback){
    this.db.get("select * from properties where refId = $refId", {$refId: refId}, function(err,row){
        callback(row);
    });
};

propertyWatcherRepository.prototype.getLatestRun = function(callback){
    this.db.get("select * from runs where runDate in (select max(runDate) from runs)", function(err,row){
        callback(row);
    });
};

propertyWatcherRepository.prototype.getPriceChangesForProperty = function(refId, callback){
    this.db.all("select * from priceChanges where refId = '"+refId.replace(/\'/g, "''")+"' order by dateChanges", function(err, rows){callback(rows)});
};

propertyWatcherRepository.prototype.close = function(){
    //console.log("closingdb");
    this.db.close();
};

module.exports = {
    propertyWatcherRepository: propertyWatcherRepository
};

