var results = [];
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,        // do not load images
        loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
    }
});
var pageNumber = 0;

casper.options.waitTimeout = 60000;

casper.start('https://espc.com/properties.aspx?ps=50', function() {
    processPage.call(this);
});

casper.run(function() {
    this.echo(JSON.stringify(results));
    this.exit();
});

var getResults = function(){   
    var parsePrice = function(price){
        var matches = price.match(/^(.*)£(.*)$/);       
        return {model: matches[1].trim(),price: parseInt(matches[2].replace(/,/g,""))};        
    }; 
    
    var parseID = function(aElement){
        var matches = aElement.match(/pid=([0-9]+)/);       
        return parseInt(matches[1]);
    }
    
    var rows = document.querySelectorAll("article.property-result");
    return Array.prototype.map.call(rows, function(row){
        try{
           var addresslines = Array.prototype.map.call(row.querySelectorAll("h2.property-result__address span"), function(span){return span.innerText.replace(",", "");});
           var price = row.querySelector("p.price").innerText;        
           var description = row.querySelector("p.quick-desc").innerText;
           var firstLink = row.querySelectorAll("a")[0];
        
           var item = {
               refId: parseID(firstLink.href),
               description: description,
               address: addresslines.join(", "),
               price: parsePrice(price).price,
               pricingModel: parsePrice(price).model,
               town: addresslines[1],
               postcode: addresslines[addresslines.length-1],
               source: "espc"
           };
           
           return item;
        }
        catch(err) {
           return null;
        }
    }).filter(function(result){return result != null});
};

var processPage = function(){    

    var that = this;

    that.waitForText("per page", function(){
        pageNumber++;
        //that.echo(pageNumber);
        var pageResults = that.evaluate(getResults);
        results = results.concat(pageResults||[]);
        
        if(that.exists("a.next")) {
            that.click("a.next");                        
            processPage.call(that)
        }  
    });
};
