var results = [];
var casper = require('casper').create({
    pageSettings: {
        loadImages:  false,        // do not load images
        loadPlugins: false         // do not load NPAPI plugins (Flash, Silverlight, ...)
    }
});
var pageNumber = 0;

casper.start('http://rack01.powering2.expertagent.co.uk/customsearch.aspx?aid={a9d512bf-391f-478b-85a1-4d44bf35d656}&sort=Desc&DefaultPage=3&dep=1&radius=25&minbeds=0&minprice=&maxprice=&placename=Dunfermline,%20Fife,%20Scotland', function() {
    processPage.call(this);
});

var getResults = function(){
   
    var parsePrice = function(price){
        var matches = price.match(/^(.*)£(.*)$/);       
        return {model: matches[1].trim(),price: parseInt(matches[2].replace(/,/g,""))};        
    };
    
    var parseId = function(href){
        var matches = href.match(/^.*?&pid=(.*?)(&page=[0-9]+)?$/);
        return matches[1];
    };   
    
    
    parseHeader = function(header){
        var lines = header.split(",").map(function(line){return line.trim()});        
        var numbers = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten"];
        var numbers2 = ["1","2","3","4","5","6","7","8","9"];
        var lastElement = lines.pop();
        var words = lastElement.split(" ");
        var description = [];
        var address = [];
        var dividerFound = false;
        
        while(words.length>0)
        {
            var word = words.shift();
            var index = numbers.indexOf(word);
            if(index > -1){
                dividerFound = true;
                word = index+1;
            } else {
                var index2 = numbers2.indexOf(word);
                if(index2 > -1){
                    dividerFound = true;
                }
            }
            
            if(dividerFound){
                description.push(word)
            }
            else {
                lines.push(word)
            }
        }
                
        return {
            address: !dividerFound?header:lines.join(", "),
            addressArray: lines,
            description: !dividerFound?header:description.join(" ")
        };
    };
        
    var rows = document.querySelectorAll(".propListItemCont");
    return Array.prototype.map.call(rows, function(row){
        
        
        var link = row.querySelector("div.propListItemTemplatePropImage>a").href;
        var refId = parseId(link);
        var header = parseHeader(row.querySelector("div.propListItemTemplateAdvertHeader").innerText);
        var address = header.address;
        var description = header.description;
        var priceRow = parsePrice(row.querySelector("div.propListItemTemplatePriceText").innerText);
//        var refId = row.querySelector("div.propListItemTemplateReference").innerText;        
        var isSold = row.querySelector("div.notOnMarket") != null;
        
        var item = {
            refId: refId,
            description: description,
            address: address,
            price: priceRow.price,
            pricingModel: priceRow.model,
            town: header.addressArray[header.addressArray.length-1]||"",
            isSold: isSold,
            postcode: "",
            source: "remax"
        };
        
        return item;
    }).filter(function(item){return item != null && item.isSold === false});;
};

var processPage = function(){  

    var that = this;

    pageNumber++;
    //that.echo(pageNumber);

    that.then(function(){
        var pageResults = that.evaluate(getResults);
        results = results.concat(pageResults||[]);
    });
    
    that.then(function(){
        if(that.exists("td.col2>a")) {
            that.thenClick("td.col2>a").then(function(){processPage.call(that)});
        }
    })

};

casper.run(function() {
    //this.echo("for sale: "+results.length);    
    this.echo(JSON.stringify(results));
    this.exit();
});
